import Graceful from '@ladjs/graceful'
import Bree from 'bree'
import stringify from 'csv-stringify'
import express from 'express'

import { getUserPrivateData } from './lib/getUserPrivateData.js'
import { timestampIsValid } from './lib/time.js'
import { getTransactionByTimestamp } from './src/models/transaction.js'
import { findUserByBlockchainAddress, findUsers, updatePendingUser } from './src/models/user.js'

const app = express()

const bree = new Bree({
  jobs: [
    {
      name: 'generateUsersCsv',
      interval: '2m'
    },
    {
      name: 'generateTransactionsCsv',
      interval: '2m'
    }
  ],
  errorHandler: (error, workerMetadata) => {
    // workerMetadata will be populated with extended worker information only if
    // Bree instance is initialized with parameter `workerMetadata: true`
    if (workerMetadata.threadId) {
      console.log(
        `There was an error while running a worker ${workerMetadata.name} with thread ID: ${workerMetadata.threadId}`
      )
    } else {
      console.log(`There was an error while running a worker ${workerMetadata.name}`)
    }

    console.log(error)
  }
})

const graceful = new Graceful({ brees: [bree] })
graceful.listen()

bree.start()

app.post('/users/:blockchain_address', async (req, res) => {
  const blockchain_address = req.params.blockchain_address
  try {
    await updatePendingUser(blockchain_address)

    res.json({
      message: 'success',
      status_code: 200
    })
  } catch (err) {
    console.log(
      `update pending user error -> ${err}, user blockchain address: ${blockchain_address}`
    )
    return res.status(500).json({
      message: 'Internal Server Error'
    })
  }
})

app.get('/users/:blockchain_address', async (req, res) => {
  const blockchain_address = req.params.blockchain_address
  try {
    const usersData = await findUserByBlockchainAddress(blockchain_address)
    let users = []
    if (usersData.length === 0) {
      res.status(404).send('User not found for blockchain address: ' + blockchain_address)
      return
    }
    users[0] = await getUserPrivateData(usersData[0])

    stringify(users, {
      header: true
    }).pipe(res)
    res.setHeader('Content-Type', 'text/csv')
    res.setHeader(
      'Content-Disposition',
      'attachment; filename="' + 'download-' + Date.now() + '.csv"'
    )
  } catch (err) {
    console.log(
      `retrieve single user error -> ${err}, user blockchain address: ${blockchain_address}`
    )
    return res.status(500).json({
      message: 'Internal Server Error'
    })
  }
})

app.get('/users', async (_, res) => {
  try {
    const usersData = await findUsers()
    let users = []
    for (let i = 0; i < usersData.length; i++) {
      users[i] = await getUserPrivateData(usersData[i])
    }
    stringify(users, {
      header: true
    }).pipe(res)
    res.setHeader('Content-Type', 'text/csv')
    res.setHeader(
      'Content-Disposition',
      'attachment; filename="' + 'download-' + Date.now() + '.csv"'
    )
  } catch (err) {
    console.log(`retrieve multiple users error -> ${err}`)
    return res.status(500).json({
      message: 'Internal Server Error'
    })
  }
})

app.get('/transactions', async (req, res) => {
  const start_timestamp = req.query.start_timestamp
  const end_timestamp = req.query.end_timestamp

  // validate timestamp format
  if (start_timestamp !== undefined) {
    const startTimestampIsValid = timestampIsValid(start_timestamp)
    if (!startTimestampIsValid) {
      return res.status(400).json({
        message: 'start_timestamp has wrong format.'
      })
    }
  }

  if (end_timestamp !== undefined) {
    const endTimestampIsValid = timestampIsValid(end_timestamp)
    if (!endTimestampIsValid) {
      return res.status(400).json({
        message: 'end_timestamp has wrong format.'
      })
    }
  }

  if (start_timestamp !== undefined && end_timestamp !== undefined) {
    if (start_timestamp > end_timestamp) {
      return res.status(400).json({
        message: 'end_timestamp cannot be earlier than start_timestamp.'
      })
    }
  }

  try {
    const transactions = await getTransactionByTimestamp(start_timestamp, end_timestamp)

    stringify(transactions, {
      header: true
    }).pipe(res)
    res.setHeader('Content-Type', 'text/csv')
    res.setHeader(
      'Content-Disposition',
      'attachment; filename="' + 'download-' + Date.now() + '.csv"'
    )
  } catch (err) {
    console.log(`retrieve multiple transactions error -> ${err}`)
    return res.status(500).json({
      message: 'Internal Server Error'
    })
  }
})

app.listen(process.env.SERVER_PORT, () => {
  console.log(`cic-data-server start at: http://localhost:${process.env.SERVER_PORT}`)
})
