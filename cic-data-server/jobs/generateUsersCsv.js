import { getUserPrivateData } from '../lib/getUserPrivateData.js'
import { uploadEncryptedFile } from '../lib/uploadEncryptedFile.js'
import { findUsers } from '../src/models/user.js'

const generateUsersCsv = async () => {
  try {
    const usersData = await findUsers()
    let users = []
    for (let i = 0; i < usersData.length; i++) {
      users[i] = await getUserPrivateData(usersData[i])
    }

    await uploadEncryptedFile('users', users)

    process.exit(0)
  } catch (err) {
    console.log(`generateUsersCsv error -> ${err}`)
    process.exit(1)
  }
}

;(async () => {
  await generateUsersCsv()
})()
