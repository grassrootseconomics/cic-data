import { uploadEncryptedFile } from '../lib/uploadEncryptedFile.js'
import { getTransactions } from '../src/models/transaction.js'

const generateTransactionsCsv = async () => {
  try {
    const transactionsData = await getTransactions()

    await uploadEncryptedFile('transactions', transactionsData)

    process.exit(0)
  } catch (err) {
    console.log(`generateTransactionsCsv error -> ${err}`)
    process.exit(1)
  }
}

;(async () => {
  await generateTransactionsCsv()
})()
