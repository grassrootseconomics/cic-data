# OpenPGP

## Generate Keys

```js
import * as openpgp from 'openpgp'
;(async () => {
  const key = await openpgp.generateKey({
    type: 'rsa', // Type of the key
    rsaBits: 4096, // RSA key size (defaults to 4096 bits)
    userIDs: [{ name: 'Max', email: 'max@example.com' }], // you can pass multiple user IDs
    passphrase: 'super long and hard to guess secret' // protects the private key
  })

  console.log(key.privateKeyArmored)
  console.log('===============')
  console.log(key.publicKeyArmored)
})()
```

## Decrypt File

```js
import fs from 'fs'
import * as openpgp from 'openpgp'

decrypt()

async function decrypt() {
  const passphrase = `super long and hard to guess secret`
  const filePath = 'downloads/encrypted-users-20210702.csv'

  const privateKey = await openpgp.decryptKey({
    privateKey: await openpgp.readKey({ armoredKey: process.env.PRIVATE_KEY }),
    passphrase
  })

  const encrypted = fs.readFileSync(filePath, 'utf8')

  const message = await openpgp.readMessage({
    armoredMessage: encrypted
  })

  const decrypted = await openpgp.decrypt({
    message,
    decryptionKeys: privateKey
  })

  console.log(decrypted.data)
}
```
