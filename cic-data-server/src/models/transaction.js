import pool from '../../lib/pg.js'
import { parseUnixTimestamp } from '../../lib/time.js'

export async function getTransactionByTimestamp(start_timestamp, end_timestamp) {
  const client = await pool.connect()
  try {
    let queryText
    if (start_timestamp === undefined && end_timestamp === undefined) {
      queryText = `SELECT * FROM transactions ORDER BY block_time, id`
    } else if (start_timestamp === undefined && end_timestamp !== undefined) {
      const end_datetime = parseUnixTimestamp(end_timestamp)
      queryText = `SELECT * FROM transactions WHERE block_time <= '${end_datetime}' ORDER BY block_time, id`
    } else if (start_timestamp !== undefined && end_timestamp === undefined) {
      const start_datetime = parseUnixTimestamp(start_timestamp)
      queryText = `SELECT * FROM transactions WHERE block_time >= '${start_datetime}' ORDER BY block_time, id`
    } else {
      const start_datetime = parseUnixTimestamp(start_timestamp)
      const end_datetime = parseUnixTimestamp(end_timestamp)
      queryText = `SELECT * FROM transactions WHERE block_time >= '${start_datetime}' AND block_time <= '${end_datetime}' ORDER BY block_time, id`
    }

    const query = await client.query(queryText)
    return query.rows
  } catch (err) {
    throw `pg getTransactionByTimestamp error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function getTransactions() {
  const client = await pool.connect()
  try {
    const queryText = `SELECT * FROM transactions ORDER BY block_time, id`
    const query = await client.query(queryText)
    return query.rows
  } catch (err) {
    throw `pg getTransactions error ->, ${err.stack}`
  } finally {
    client.release()
  }
}
