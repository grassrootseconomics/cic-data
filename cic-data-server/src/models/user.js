import pool from '../../lib/pg.js'

export async function updatePendingUser(blockchain_address) {
  const client = await pool.connect()
  try {
    await client.query(
      `UPDATE users SET pending_update = true WHERE blockchain_address = '${blockchain_address}'`
    )
  } catch (err) {
    throw `pg updatePendingUser error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function findUserByBlockchainAddress(blockchain_address) {
  const client = await pool.connect()
  try {
    const query = await client.query(
      `SELECT * FROM users WHERE blockchain_address = '${blockchain_address}'`
    )
    return query.rows
  } catch (err) {
    throw `pg findUserByBlockchainAddress error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function findUsers() {
  const client = await pool.connect()
  try {
    const query = await client.query(`SELECT * FROM users ORDER BY id`)
    return query.rows
  } catch (err) {
    throw `pg findUsers error ->, ${err.stack}`
  } finally {
    client.release()
  }
}
