import fs from 'fs/promises'

export const writeFile = async (fileName, stringifyData) => {
  try {
    await fs.writeFile('downloads/' + fileName, stringifyData, {
      flag: 'w'
    })
  } catch (err) {
    throw `write file error -> ${err}`
  }
}

export const deleteFile = async fileName => {
  try {
    await fs.unlink('downloads/' + fileName)
  } catch (err) {
    console.log(`delete file error -> ${err}`)
  }
}
