import pg from 'pg'

// Working with PostgreSQL's timestamp without timezone in Node
// (https://60devs.com/working-with-postgresql-timestamp-without-timezone-in-node.html)
pg.types.setTypeParser(pg.types.builtins.TIMESTAMP, str => str)
pg.types.setTypeParser(20, BigInt) // Type Id 20 = BIGINT | BIGSERIAL

const Pool = pg.Pool

const pool = new Pool({
  database: process.env.DB_NAME,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  max: 20
})

export default pool
