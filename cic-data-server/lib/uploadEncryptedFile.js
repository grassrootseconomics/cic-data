import stringify from 'csv-stringify/lib/sync.js'

import { encrypt } from './encrypt.js'
import { deleteFile, writeFile } from './fs.js'
import { uploadFileToSftp } from './sftp.js'
import { getTodayDate } from './time.js'

export const uploadEncryptedFile = async (file_prefix, data) => {
  const todayDate = getTodayDate()
  const fileName = `${file_prefix}-${todayDate}.csv`
  const encryptedFileName = `encrypted-${fileName}`

  try {
    const usersStringifyData = await stringify(data, { header: true })
    await writeFile(fileName, usersStringifyData)
    await encrypt(fileName)
    await uploadFileToSftp(encryptedFileName)
  } catch (err) {
    throw `encrypt and upload file error -> ${err}`
  } finally {
    await deleteFile(fileName)
    await deleteFile(encryptedFileName)
  }
}
