import fs from 'fs'
import * as openpgp from 'openpgp'

export const encrypt = async fileName => {
  try {
    const publicKey = await openpgp.readKey({ armoredKey: process.env.PUBLIC_KEY })
    const plainData = fs.readFileSync(`downloads/${fileName}`, 'utf8')
    const encrypted = await openpgp.encrypt({
      message: await openpgp.createMessage({
        text: plainData
      }),
      encryptionKeys: publicKey
    })

    fs.writeFileSync(`downloads/encrypted-${fileName}`, encrypted)
  } catch (err) {
    throw `encrypt data error -> ${err}`
  }
}
