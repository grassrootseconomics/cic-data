import { toAddressKey } from '@cicnet/crdt-meta'
import atob from 'atob'
import { parse } from 'vcard-parser'

import fetcher from './fetcher.js'

const metaServer = `http://${process.env.META_SERVER_HOST}:${process.env.META_SERVER_PORT}`

export const getUser = async blockchain_address => {
  try {
    return await getUserData(metaServer, blockchain_address)
  } catch (err) {
    throw `web3 getUser meta error -> ${err}`
  }
}

async function getUserData(metaServer, blockchainAddress) {
  const accountKey = await toAddressKey(blockchainAddress, ':cic.person')
  const accountData = await fetcher(`${metaServer}/${accountKey}`)
  const vcardData = parse(atob(accountData.vcard))
  accountData.vcard = vcardData
  return accountData
}
