import fs from 'fs'
import SFTPClient from 'ssh2-sftp-client'

export const uploadFileToSftp = async fileName => {
  try {
    let sftp = await new SFTPClient()

    const localFileLocation = fs.createReadStream('downloads/' + fileName)
    const remoteFileLocation = 'uploads/' + fileName
    const config = {
      host: process.env.SFTP_ADDRESS,
      port: process.env.SFTP_PORT,
      username: process.env.SFTP_ACCOUNT,
      password: process.env.SFTP_PASSWORD,
      writeStreamOptions: {
        flags: 'w',
        encoding: null,
        mode: 0o666,
        autoClose: true
      }
    }

    await sftp.connect(config)
    await sftp.put(localFileLocation, remoteFileLocation)
    await sftp.end()
  } catch (err) {
    throw `sftp upload error -> ${err}`
  }
}
