import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

/**
 * @example
 * // returns 2019-07-10 22:35:55.000000
 * parseUnixTimestamp(1562798155);
 * @param {number} unixTimestamp
 * @returns {string}
 */
export const parseUnixTimestamp = unixTimestamp => {
  return dayjs.unix(unixTimestamp).utc().format()
}

/**
 * Returns current time in UTC.
 * @returns {string}
 */
export const formatNow = () => dayjs().utc().format()

export const timestampIsValid = timestamp => {
  return dayjs(timestamp).isValid()
}

export const getTodayDate = () => {
  return dayjs().format('YYYYMMDD')
}
