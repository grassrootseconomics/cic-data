import { jest } from '@jest/globals'

import * as time from './time'

jest.useFakeTimers('modern')

beforeAll(() => {
  jest.setSystemTime(new Date('2000-01-01T00:00:00Z').getTime())
})

test('should convert Unix timestamp to ISO 8601 format', () => {
  expect(time.parseUnixTimestamp(1621951302)).toBe('2021-05-25T14:01:42Z')
})

test('should convert current time to ISO 8601 format', () => {
  expect(time.formatNow()).toBe('2000-01-01T00:00:00Z')
})

afterAll(() => {
  jest.useRealTimers()
})
