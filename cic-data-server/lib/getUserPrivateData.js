import _ from 'lodash'

import { getUser } from './meta.js'

export async function getUserPrivateData(user) {
  try {
    const userPrivateData = await getUser(user.blockchain_address)
    user.first_name = _.get(userPrivateData, 'vcard.n[0].value[0]') || null
    user.last_name = _.get(userPrivateData, 'vcard.n[0].value[1]') || null
    user.phone = _.get(userPrivateData, 'vcard.tel[0].value') || null
    user.product = _.get(userPrivateData, 'products[0]') || null
    user.location = _.get(userPrivateData, 'location.area_name') || null
    user.user_url = process.env.USER_BASE_URL + user.blockchain_address.substring(2)
    return user
  } catch (err) {
    throw `getUserPrivateData error -> ${err}, user blockchain address: ${user.blockchain_address}`
  }
}
