# cic-data-server

Query aggregated data views. (Re)-indexes data on-demand when triggered through HTTP API.
