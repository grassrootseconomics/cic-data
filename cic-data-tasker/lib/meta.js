import { toAddressKey } from '@cicnet/crdt-meta'
import axios from 'axios'
import { parse } from 'vcard-parser'

import fetcher from './fetcher.js'

const metaServer = `http://${process.env.META_SERVER_HOST}:${process.env.META_SERVER_PORT}`

const getAddressMetaData = async (address, identifier) => {
  try {
    return await toAddressKey(address, identifier)
  } catch (err) {
    throw `meta getAddressMetaData error -> ${err}`
  }
}

const getUserData = async (metaServer, blockchainAddress) => {
  const accountKey = await getAddressMetaData(blockchainAddress, ':cic.person')
  const accountData = await fetcher(`${metaServer}/${accountKey}`)
  const vcardData = parse(atob(accountData.vcard))
  accountData.vcard = vcardData
  return accountData
}

export const getUser = async blockchain_address => {
  try {
    return await getUserData(metaServer, blockchain_address)
  } catch (err) {
    throw `web3 getUser meta error -> ${err}`
  }
}

export const getAccountType = async blockchainAddress => {
  const custom = await getAddressMetaData(blockchainAddress, ':cic.custom')

  let accountType
  try {
    const getAccountTypeResponse = await axios.get(`${metaServer}/${custom}`)
    accountType = getAccountTypeResponse.data.tags[0]
  } catch (err) {
    if (err.response && err.response.status === 404) {
      accountType = 'individual'
    } else {
      throw `get AccountType Error -> ${err}`
    }
  }

  return accountType
}

export const getPreferences = async blockchainAddress => {
  const preferences = await getAddressMetaData(blockchainAddress, ':cic.preferences')

  let preferredLanguage
  let isMarketEnabled
  try {
    const getPreferencesResponse = await axios.get(`${metaServer}/${preferences}`)
    preferredLanguage = getPreferencesResponse.data.preferred_language
    isMarketEnabled = getPreferencesResponse.data.is_market_enabled
  } catch (err) {
    if (err.response && err.response.status === 404) {
      preferredLanguage = 'sw'
      isMarketEnabled = false
    } else {
      throw `get Preferences Error -> ${err}`
    }
  }

  return { preferred_language: preferredLanguage, is_market_enabled: isMarketEnabled }
}
