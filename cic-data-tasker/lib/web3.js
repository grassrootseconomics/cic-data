import {
  getAccountsAddresses,
  getAddressFromRegistry,
  getTokenBalance,
  getUserData,
  TokenContract,
  Web3Instance
} from '@cicnet/meta-reader'

export class Web3 {
  static singleton

  constructor() {
    this.instance = Web3Instance(
      `ws://${process.env.WEB3DRIVER_HOST}:${process.env.WEB3DRIVER_PORT}`
    )
    console.log('Connected to web3 driver')
  }

  static getInstance() {
    if (Web3.singleton === undefined) {
      Web3.singleton = new Web3()
      Web3.singleton.instance.release = () => {
        Web3.singleton.instance.currentProvider.connection.close()
        Web3.singleton = undefined
        console.log('Disconnected from web3 driver')
      }
    }

    return Web3.singleton.instance
  }
}

export const getAllUsers = async web3Instance => {
  const metaServer = `http://${process.env.META_SERVER_HOST}:${process.env.META_SERVER_PORT}`
  let allAccounts = []

  try {
    const accountIndexAddress = await getAddressFromRegistry(
      'AccountRegistry',
      process.env.ACCOUNT_REGISTRY_ADDRESS,
      web3Instance
    )
    const accountsAddresses = await getAccountsAddresses(accountIndexAddress, web3Instance)
    let accountCount = 1
    console.log(`Received ${accountsAddresses.length} accounts`)

    for (const account of accountsAddresses) {
      const userData = await getUserData(metaServer, account)

      if (accountCount % 100 == 0) console.log(`Processed account number ${accountCount}`)
      accountCount++

      allAccounts.push(userData)
    }
    return allAccounts
  } catch (err) {
    throw `web3 getAllUsers error -> ${err}`
  }
}

export const getUserTokenBalance = async (web3Instance, blockchain_address) => {
  const tokenContract = TokenContract(process.env.TOKEN_CONTRACT_ADDRESS, web3Instance)
  try {
    return await getTokenBalance(blockchain_address, tokenContract)
  } catch (err) {
    throw `web3 getUserBalanceToken error -> ${err}`
  }
}
