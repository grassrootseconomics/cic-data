import { getKeyByValue } from './keyValueMap.js'

const mapObject = {
  area_types: {
    other: ['other'],
    periurban: ['kilifi', 'periurban'],
    rural: [
      'kakuma',
      'kinango',
      'kinango kwale',
      'kitui',
      'kwale',
      'misc rural counties',
      'nyanza',
      'rural'
    ],
    urban: ['kisauni', 'misc mombasa', 'misc nairobi', 'mukuru nairobi', 'urban']
  }
}

test('should return correctly matched key for supplied value', () => {
  const area_type = getKeyByValue(mapObject['area_types'], 'kitui')

  expect(area_type).toBe('rural')
})

test('should return undefined for unmatchable value', () => {
  const area_type = getKeyByValue(mapObject['area_types'], 'nowhere')

  expect(area_type).toBe(undefined)
})
