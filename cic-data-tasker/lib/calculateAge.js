import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

export const calculateAge = date_of_birth => {
  const year = date_of_birth.year || 0
  const month = date_of_birth.month || 1
  const day = date_of_birth.day || 1
  const today = dayjs().utc()

  if (year === 0) {
    return 0
  }

  const user_birthday = dayjs()
    .set('year', year)
    .set('month', month - 1) // Month (January as 0, December as 11)
    .set('date', day)

  return Math.floor(today.diff(user_birthday, 'year', true))
}
