import { jest } from '@jest/globals'

import { calculateDaysEnrolled } from './calculateDaysEnrolled.js'

jest.useFakeTimers('modern')

beforeAll(() => {
  jest.setSystemTime(new Date('2021-05-30T01:00:00Z').getTime())
})

// tool to convert utc to timestamp
// https://www.browserling.com/tools/utc-to-unix

test('should return zero day', () => {
  // 2021-05-29 02:00:00 = 1622253600
  expect(calculateDaysEnrolled(1622253600)).toBe(0)
})

test('should return one day', () => {
  // 2021-05-29 00:00:00 = 1622246400
  expect(calculateDaysEnrolled(1622246400)).toBe(1)
})

test('should return thirty days', () => {
  // 2021-04-30 00:00:00 = 1619740800
  expect(calculateDaysEnrolled(1619740800)).toBe(30)
})

afterAll(() => {
  jest.useRealTimers()
})
