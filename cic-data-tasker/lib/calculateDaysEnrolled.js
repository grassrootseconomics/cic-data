import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc.js'

dayjs.extend(utc)

export const calculateDaysEnrolled = date_registered => {
  const today = dayjs().utc()

  const enrolledDay = dayjs.unix(date_registered)

  let hours = today.diff(enrolledDay, 'hours')

  return Math.floor(hours / 24)
}
