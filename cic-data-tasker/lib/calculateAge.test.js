import { jest } from '@jest/globals'

import { calculateAge } from './calculateAge.js'

jest.useFakeTimers('modern')

beforeAll(() => {
  jest.setSystemTime(new Date('2020-05-30T00:00:00Z').getTime())
})

test('should return age zero', () => {
  expect(calculateAge({})).toBe(0)
})

test('should return age zero without year', () => {
  expect(calculateAge({ month: 5, day: 30 })).toBe(0)
})

test('should return correctly matched age without day', () => {
  expect(calculateAge({ year: 2000, month: 6 })).toBe(19)
})

test('should return correctly matched age without month', () => {
  expect(calculateAge({ year: 2000, day: 30 })).toBe(20)
})

test('should return correctly matched age', () => {
  expect(calculateAge({ year: 2000, month: 5, day: 31 })).toBe(19)
  expect(calculateAge({ year: 2000, month: 5, day: 29 })).toBe(20)
})

afterAll(() => {
  jest.useRealTimers()
})
