import Graceful from '@ladjs/graceful'
import Bree from 'bree'

const bree = new Bree({
  jobs: [
    {
      name: 'bootstrapping'
    },
    {
      name: 'updateTransactions',
      interval: '1m'
    },
    {
      name: 'updateUsers',
      interval: '1m'
    }
  ],
  errorHandler: (error, workerMetadata) => {
    // workerMetadata will be populated with extended worker information only if
    // Bree instance is initialized with parameter `workerMetadata: true`
    if (workerMetadata.threadId) {
      console.log(
        `There was an error while running a worker ${workerMetadata.name} with thread ID: ${workerMetadata.threadId}`
      )
    } else {
      console.log(`There was an error while running a worker ${workerMetadata.name}`)
    }

    console.log(error)
  }
})

const graceful = new Graceful({ brees: [bree] })
graceful.listen()

bree.run('bootstrapping')
bree.workers['bootstrapping'].on('message', event => {
  console.log(`Received message from worker: ${event}`)
  if (event === 'done') {
    bree.start('updateTransactions')
    bree.start('updateUsers')
  }
})
