import axios from 'axios'
import _ from 'lodash'
import { parentPort } from 'worker_threads'

import { getAllUsers, Web3 } from '../lib/web3.js'
import * as transactionService from '../src/services/transaction.js'
import * as userService from '../src/services/user.js'

const bootstrapping = async () => {
  const web3Instance = Web3.getInstance()

  try {
    const users = await getAllUsers(web3Instance)

    await userService.processUsers(users)

    const getTransactionResponse = await axios.get(
      `http://${process.env.DATA_PROVIDER_TRANSACTIONS_HOST}:${process.env.DATA_PROVIDER_TRANSACTIONS_PORT}/txa/0/${process.env.BOOTSTRAPPING_END_BLOCK_NUMBER}`,
      {
        headers: {
          'X-CIC-CACHE-MODE': 'all'
        }
      }
    )

    const transactions = (_.get(getTransactionResponse, 'data.data') || [])
      .filter(t => t.success === true)
      .sort((a, b) => a.block_number - b.block_number)

    await transactionService.processTransactions(transactions)

    // To close out the worker and signal that it is done, you can simply process.exit(0).
    if (parentPort) {
      parentPort.postMessage('done')
    } else {
      process.exit(0)
    }
  } catch (err) {
    console.log('bootstrapping error ->', err)
    process.exit(1)
  } finally {
    web3Instance.release()
  }
}

;(async () => {
  await bootstrapping()
})()
