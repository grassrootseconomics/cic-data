import axios from 'axios'
import _ from 'lodash'

import * as transactionModel from '../src/models/transaction.js'
import * as transactionService from '../src/services/transaction.js'
import * as userService from '../src/services/user.js'

const updateTransactions = async () => {
  try {
    const latestBlockNum = await transactionModel.selectHighestBlock()

    // TODO: discuss swapping the order of these two async calls

    await userService.updatePendingUser()

    const start_block = Math.max(
      0,
      latestBlockNum - parseInt(process.env.UPDATE_TRANSACTION_BLOCK_DECREASE_NUMBER)
    )
    const end_block =
      latestBlockNum + parseInt(process.env.UPDATE_TRANSACTION_BLOCK_INCREASE_NUMBER)

    const getTransactionResponse = await axios.get(
      `http://${process.env.DATA_PROVIDER_TRANSACTIONS_HOST}:${process.env.DATA_PROVIDER_TRANSACTIONS_PORT}/txa/${start_block}/${end_block}`,
      {
        headers: {
          'X-CIC-CACHE-MODE': 'all'
        }
      }
    )

    const transactions = (_.get(getTransactionResponse, 'data.data') || [])
      .filter(t => t.success === true)
      .sort((a, b) => a.block_number - b.block_number)

    await transactionService.processTransactions(transactions)

    // To close out the worker and signal that it is done, you can simply process.exit(0).
    process.exit(0)
  } catch (err) {
    console.log('updateTransactions error ->', err)
    process.exit(1)
  }
}

;(async () => {
  await updateTransactions()
})()
