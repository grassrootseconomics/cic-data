import * as userService from '../src/services/user.js'

const updateUsers = async () => {
  try {
    await userService.updateUserTokenBalance()
    // To close out the worker and signal that it is done, you can simply process.exit(0).
    process.exit(0)
  } catch (err) {
    console.log('updateUsers error ->', err)
    process.exit(1)
  }
}

;(async () => {
  await updateUsers()
})()
