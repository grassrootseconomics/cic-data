import _ from 'lodash'

import { calculateAge } from '../../lib/calculateAge.js'
import { calculateDaysEnrolled } from '../../lib/calculateDaysEnrolled.js'
import { getKeyByValue } from '../../lib/keyValueMap.js'
import * as time from '../../lib/time.js'

const userEntity = (
  blockchain_address,
  date_registered,
  days_enrolled,
  product_category,
  gender,
  age,
  area_name,
  area_type,
  account_type,
  preferred_language,
  is_market_enabled,
  token_balance,
  ovol_in,
  ovol_out,
  otxns_in,
  otxns_out,
  ounique_in,
  ounique_out,
  svol_in,
  svol_out,
  stxns_in,
  stxns_out,
  sunique_in,
  sunique_out,
  sunique_all,
  last_trade_out,
  first_trade_in_user,
  first_trade_in_time
) => ({
  blockchain_address,
  date_registered,
  days_enrolled,
  product_category,
  gender,
  age,
  area_name,
  area_type,
  account_type,
  preferred_language,
  is_market_enabled,
  token_balance,
  ovol_in,
  ovol_out,
  otxns_in,
  otxns_out,
  ounique_in,
  ounique_out,
  svol_in,
  svol_out,
  stxns_in,
  stxns_out,
  sunique_in,
  sunique_out,
  sunique_all,
  last_trade_out,
  first_trade_in_user,
  first_trade_in_time
})

export const toUserEntity = (user, area_names, area_types, product_categories) => {
  const area_name = getKeyByValue(area_names, user.location.area_name) || 'unknown'
  const area_type = getKeyByValue(area_types, area_name) || 'unknown'
  const product_category = getKeyByValue(product_categories, user.products[0]) || 'unknown'
  const age = calculateAge(_.get(user, 'date_of_birth') || { year: 0, month: 0, day: 0 })
  const days_enrolled = calculateDaysEnrolled(user.date_registered) || 0

  return userEntity(
    user.identities['evm']['bloxberg:8996'][0],
    time.parseUnixTimestamp(user.date_registered),
    days_enrolled,
    product_category,
    user.gender,
    age,
    area_name,
    area_type,
    _.get(user, 'account_type') || 'individual',
    _.get(user, 'preferred_language') || 'en',
    _.get(user, 'is_market_enabled') || false,
    _.toInteger(_.get(user, 'token_balance') || 0),
    BigInt(_.get(user, 'ovol_in') || 0),
    BigInt(_.get(user, 'ovol_out') || 0),
    _.toInteger(_.get(user, 'otxns_in') || 0),
    _.toInteger(_.get(user, 'otxns_out') || 0),
    _.toInteger(_.get(user, 'ounique_in') || 0),
    _.toInteger(_.get(user, 'ounique_out') || 0),
    BigInt(_.get(user, 'svol_in') || 0),
    BigInt(_.get(user, 'svol_out') || 0),
    _.toInteger(_.get(user, 'stxns_in') || 0),
    _.toInteger(_.get(user, 'stxns_out') || 0),
    _.toInteger(_.get(user, 'sunique_in') || 0),
    _.toInteger(_.get(user, 'sunique_out') || 0),
    _.toInteger(_.get(user, 'sunique_all') || 0),
    _.get(user, 'last_trade_out') || null,
    _.get(user, 'first_trade_in_user') || null,
    _.get(user, 'first_trade_in_time') || null
  )
}
