import pool from '../../lib/pg.js'
import * as time from '../../lib/time.js'

export async function findUserByBlockchainAddress(blockchain_address) {
  const client = await pool.connect()
  try {
    if (
      blockchain_address === process.env.TRUST_ADDRESS ||
      blockchain_address === process.env.FAUCET_ADDRESS
    ) {
      return {
        blockchain_address: 'SYSTEM_ADDRESS',
        date_registered: '1970-01-01 00:00:00',
        age: 0,
        product_category: 'system',
        area_type: 'system',
        area_name: 'system',
        gender: 'system'
      }
    } else {
      const query = await client.query('SELECT * FROM users WHERE blockchain_address = $1', [
        blockchain_address
      ])
      return query.rows[0] || null
    }
  } catch (err) {
    throw `pg findUserByBlockchainAddress error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function findPendingUsers() {
  const client = await pool.connect()
  try {
    const query = await client.query('SELECT * FROM users WHERE pending_update = true')
    return query.rows
  } catch (err) {
    throw `pg findPendingUsers error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function findUsers() {
  const client = await pool.connect()
  try {
    const query = await client.query('SELECT * FROM users')
    return query.rows
  } catch (err) {
    throw `pg findUsers error ->, ${err.stack}`
  } finally {
    client.release()
  }
}

export async function createUser(userEntity) {
  const text = `INSERT INTO users(
        blockchain_address,
        date_registered,
        days_enrolled,
        product_category,
        gender,
        age,
        area_name,
        area_type,
        account_type,
        preferred_language,
        is_market_enabled,
        updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`
  const client = await pool.connect()
  try {
    return await client.query(text, [
      userEntity.blockchain_address,
      userEntity.date_registered,
      userEntity.days_enrolled,
      userEntity.product_category,
      userEntity.gender,
      userEntity.age,
      userEntity.area_name,
      userEntity.area_type,
      userEntity.account_type,
      userEntity.preferred_language,
      userEntity.is_market_enabled,
      time.formatNow()
    ])
  } catch (err) {
    throw `pg createUser -> ${err.stack}`
  } finally {
    client.release()
  }
}

export function updateUser(client, userEntity) {
  const text = `UPDATE users SET
      date_registered = $1,
      days_enrolled = $2,
      product_category = $3,
      gender = $4,
      age = $5,
      area_name = $6,
      area_type = $7,
      account_type = $8,
      preferred_language = $9,
      is_market_enabled = $10,
      pending_update = $11,
      token_balance = $12,
      ovol_in = $13,
      ovol_out = $14,
      otxns_in = $15,
      otxns_out = $16,
      ounique_in = $17,
      ounique_out = $18,
      svol_in = $19,
      svol_out = $20,
      stxns_in = $21,
      stxns_out = $22,
      sunique_in = $23,
      sunique_out = $24,
      sunique_all = $25,
      last_trade_out = $26,
      first_trade_in_user = $27,
      first_trade_in_time = $28,
      updated_at = $29
      WHERE blockchain_address = '${userEntity.blockchain_address}'`
  try {
    return client.query(text, [
      userEntity.date_registered,
      userEntity.days_enrolled,
      userEntity.product_category,
      userEntity.gender,
      userEntity.age,
      userEntity.area_name,
      userEntity.area_type,
      userEntity.account_type,
      userEntity.preferred_language,
      userEntity.is_market_enabled,
      false,
      userEntity.token_balance,
      userEntity.ovol_in,
      userEntity.ovol_out,
      userEntity.otxns_in,
      userEntity.otxns_out,
      userEntity.ounique_in,
      userEntity.ounique_out,
      userEntity.svol_in,
      userEntity.svol_out,
      userEntity.stxns_in,
      userEntity.stxns_out,
      userEntity.sunique_in,
      userEntity.sunique_out,
      userEntity.sunique_all,
      userEntity.last_trade_out,
      userEntity.first_trade_in_user,
      userEntity.first_trade_in_time,
      time.formatNow()
    ])
  } catch (err) {
    throw `pg updateUser -> ${err.stack}`
  }
}

export function updateUserTokenBalance(client, userEntity) {
  const text = `UPDATE users SET token_balance = $1, updated_at = $2 WHERE blockchain_address = '${userEntity.blockchain_address}'`
  try {
    return client.query(text, [userEntity.token_balance, time.formatNow()])
  } catch (err) {
    throw `pg updateUserTokenBalance -> ${err.stack}`
  }
}
