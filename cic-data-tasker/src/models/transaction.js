import _ from 'lodash'

import pool from '../../lib/pg.js'

export async function findTransaction(transaction) {
  const client = await pool.connect()
  try {
    const queryResult = await client.query(
      'SELECT * FROM transactions WHERE transaction_hash = $1',
      [transaction.tx_hash]
    )
    return _.get(queryResult, 'rows[0]') || null
  } catch (err) {
    throw `pg findTransaction error -> ${err.stack}`
  } finally {
    client.release()
  }
}

export async function selectHighestBlock() {
  const client = await pool.connect()
  try {
    const queryResult = await client.query('SELECT MAX("block_number") FROM transactions')
    return _.get(queryResult, 'rows[0].max') || 0
  } catch (err) {
    throw `pg selectHighestBlock error -> ${err.stack}`
  } finally {
    client.release()
  }
}

export async function isTransactionUnique(sender_address, receiver_address) {
  const client = await pool.connect()
  try {
    const query = await client.query(
      'SELECT count(sender_address) FROM transactions WHERE sender_address = $1 AND receiver_address = $2',
      [sender_address, receiver_address]
    )
    return query.rows[0].count == 0
  } catch (err) {
    throw `pg isTransactionUnique error -> ${err.stack}`
  } finally {
    client.release()
  }
}

export function createTransaction(client, transactionEntity) {
  const insertTransactions = `INSERT INTO transactions(
        transaction_hash, 
        block_number, 
        block_time, 
        sender_address, 
        receiver_address, 
        amount_sent, 
        amount_received, 
        transaction_type, 
        sender_token, 
        receiver_token, 
        sender_registered, 
        sender_product_category, 
        sender_gender, 
        sender_age, 
        sender_area_name, 
        sender_area_type, 
        receiver_registered, 
        receiver_product_category, 
        receiver_gender, 
        receiver_age, 
        receiver_area_name, 
        receiver_area_type
      ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22)`

  const transactionValues = [
    transactionEntity.transaction_hash,
    transactionEntity.block_number,
    transactionEntity.block_time,
    transactionEntity.sender_address,
    transactionEntity.receiver_address,
    transactionEntity.amount_sent,
    transactionEntity.amount_received,
    transactionEntity.transaction_type,
    transactionEntity.sender_token,
    transactionEntity.receiver_token,
    transactionEntity.sender_registered,
    transactionEntity.sender_product_category,
    transactionEntity.sender_gender,
    transactionEntity.sender_age,
    transactionEntity.sender_area_name,
    transactionEntity.sender_area_type,
    transactionEntity.receiver_registered,
    transactionEntity.receiver_product_category,
    transactionEntity.receiver_gender,
    transactionEntity.receiver_age,
    transactionEntity.receiver_area_name,
    transactionEntity.receiver_area_type
  ]
  try {
    return client.query(insertTransactions, transactionValues)
  } catch (err) {
    throw `pg createTransaction error -> ${err.stack}`
  }
}
