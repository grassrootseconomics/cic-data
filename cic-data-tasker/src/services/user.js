import axios from 'axios'
import _ from 'lodash'

import { getAccountType, getPreferences, getUser } from '../../lib/meta.js'
import pool from '../../lib/pg.js'
import { getUserTokenBalance, Web3 } from '../../lib/web3.js'
import * as userModel from '../models/user.js'
import { toUserEntity } from '../models/userEntity.js'

export const getUserResource = async () => {
  const getAreaNameResponse = await axios.get(`${process.env.AREA_NAME_HOST}`)
  const getAreaTypeResponse = await axios.get(`${process.env.AREA_TYPE_HOST}`)
  const getProductCategoryResponse = await axios.get(`${process.env.PRODUCT_CATEGORY_HOST}`)

  return {
    area_names: getAreaNameResponse.data['area_names'],
    area_types: getAreaTypeResponse.data['area_types'],
    product_categories: getProductCategoryResponse.data['product_categories']
  }
}

export const processUsers = async users => {
  try {
    const userResources = await getUserResource()
    let userCount = 1
    console.log(`Received ${users.length} users`)

    for (const user of users) {
      const blockchainAddress = user.identities['evm']['bloxberg:8996'][0]
      // search for existing user
      const searchUserData = await userModel.findUserByBlockchainAddress(blockchainAddress)

      if (userCount % 100 == 0) console.log(`Processed user number ${userCount}`)
      userCount++

      // if user data already exists, don't create again
      if (searchUserData != null) {
        continue
      }

      // get User account Type
      user.account_type = await getAccountType(blockchainAddress)

      // get User Preferences
      const userPreferences = await getPreferences(blockchainAddress)
      user.preferred_language = userPreferences.preferred_language
      user.is_market_enabled = userPreferences.is_market_enabled

      const userEntity = toUserEntity(
        user,
        userResources.area_names,
        userResources.area_types,
        userResources.product_categories
      )

      await userModel.createUser(userEntity)
    }
  } catch (err) {
    throw `processUsers error -> ${err}`
  }
}

export const updatePendingUser = async () => {
  const client = await pool.connect()

  try {
    const pendingUsers = await userModel.findPendingUsers()
    const userResources = await getUserResource()

    for (const pendingUser of pendingUsers) {
      const user = await getUser(pendingUser.blockchain_address)
      const userEntity = toUserEntity(
        user,
        userResources.area_names,
        userResources.area_types,
        userResources.product_categories
      )
      await userModel.updateUser(client, userEntity)
    }
  } catch (err) {
    throw `updatePendingUser error -> ${err}`
  } finally {
    client.release()
  }
}

export const updateUserTokenBalance = async () => {
  const client = await pool.connect()
  const web3Instance = Web3.getInstance()

  try {
    const users = await userModel.findUsers()
    for (const user of users) {
      const userTokenBalance = await getUserTokenBalance(web3Instance, user.blockchain_address)
      user.token_balance = _.toInteger(userTokenBalance) || 0
      await userModel.updateUserTokenBalance(client, user)
    }
  } catch (err) {
    throw `updateUserTokenBalance error -> ${err}`
  } finally {
    client.release()
    web3Instance.release()
  }
}
