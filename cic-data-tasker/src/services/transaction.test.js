import { attachTransactionInfoToUser } from './transaction.js'

describe('attach transaction info to user', () => {
  test('1) standard transaction, unique transaction = true', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.ovol_in).toBe(10000n)
    expect(senderEntity.ovol_out).toBe(12000n)
    expect(senderEntity.otxns_in).toBe(5)
    expect(senderEntity.otxns_out).toBe(10)
    expect(senderEntity.ounique_in).toBe(1)
    expect(senderEntity.ounique_out).toBe(2)
    expect(senderEntity.svol_in).toBe(40000n)
    expect(senderEntity.svol_out).toBe(1069063000n)
    expect(senderEntity.stxns_in).toBe(3)
    expect(senderEntity.stxns_out).toBe(7)
    expect(senderEntity.sunique_in).toBe(2)
    expect(senderEntity.sunique_out).toBe(4)
    expect(senderEntity.sunique_all).toBe(9)
    expect(recipientEntity.ovol_in).toBe(50000n)
    expect(recipientEntity.ovol_out).toBe(92800n)
    expect(recipientEntity.otxns_in).toBe(10)
    expect(recipientEntity.otxns_out).toBe(5)
    expect(recipientEntity.ounique_in).toBe(3)
    expect(recipientEntity.ounique_out).toBe(1)
    expect(recipientEntity.svol_in).toBe(1069083200n)
    expect(recipientEntity.svol_out).toBe(102300n)
    expect(recipientEntity.stxns_in).toBe(9)
    expect(recipientEntity.stxns_out).toBe(3)
    expect(recipientEntity.sunique_in).toBe(5)
    expect(recipientEntity.sunique_out).toBe(2)
    expect(recipientEntity.sunique_all).toBe(11)
  })

  test('2) standard transaction, unique transaction = false', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = false
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.ovol_in).toBe(10000n)
    expect(senderEntity.ovol_out).toBe(12000n)
    expect(senderEntity.otxns_in).toBe(5)
    expect(senderEntity.otxns_out).toBe(10)
    expect(senderEntity.ounique_in).toBe(1)
    expect(senderEntity.ounique_out).toBe(2)
    expect(senderEntity.svol_in).toBe(40000n)
    expect(senderEntity.svol_out).toBe(1069063000n)
    expect(senderEntity.stxns_in).toBe(3)
    expect(senderEntity.stxns_out).toBe(7)
    expect(senderEntity.sunique_in).toBe(2)
    expect(senderEntity.sunique_out).toBe(3)
    expect(senderEntity.sunique_all).toBe(8)
    expect(recipientEntity.ovol_in).toBe(50000n)
    expect(recipientEntity.ovol_out).toBe(92800n)
    expect(recipientEntity.otxns_in).toBe(10)
    expect(recipientEntity.otxns_out).toBe(5)
    expect(recipientEntity.ounique_in).toBe(3)
    expect(recipientEntity.ounique_out).toBe(1)
    expect(recipientEntity.svol_in).toBe(1069083200n)
    expect(recipientEntity.svol_out).toBe(102300n)
    expect(recipientEntity.stxns_in).toBe(9)
    expect(recipientEntity.stxns_out).toBe(3)
    expect(recipientEntity.sunique_in).toBe(4)
    expect(recipientEntity.sunique_out).toBe(2)
    expect(recipientEntity.sunique_all).toBe(10)
  })

  test('3) disbursement transaction, unique transaction = true', () => {
    const senderEntity = {
      blockchain_address: 'SYSTEM_ADDRESS'
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: 'SYSTEM_ADDRESS',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'disbursement'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(recipientEntity.ovol_in).toBe(1069050000n)
    expect(recipientEntity.ovol_out).toBe(92800n)
    expect(recipientEntity.otxns_in).toBe(11)
    expect(recipientEntity.otxns_out).toBe(5)
    expect(recipientEntity.ounique_in).toBe(4)
    expect(recipientEntity.ounique_out).toBe(1)
    expect(recipientEntity.svol_in).toBe(83200n)
    expect(recipientEntity.svol_out).toBe(102300n)
    expect(recipientEntity.stxns_in).toBe(8)
    expect(recipientEntity.stxns_out).toBe(3)
    expect(recipientEntity.sunique_in).toBe(4)
    expect(recipientEntity.sunique_out).toBe(2)
    expect(recipientEntity.sunique_all).toBe(10)
  })

  test('4) disbursement transaction, unique transaction = false', () => {
    const senderEntity = {
      blockchain_address: 'SYSTEM_ADDRESS'
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: 'SYSTEM_ADDRESS',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'disbursement'
    }
    const isTransactionUnique = false
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(recipientEntity.ovol_in).toBe(1069050000n)
    expect(recipientEntity.ovol_out).toBe(92800n)
    expect(recipientEntity.otxns_in).toBe(11)
    expect(recipientEntity.otxns_out).toBe(5)
    expect(recipientEntity.ounique_in).toBe(3)
    expect(recipientEntity.ounique_out).toBe(1)
    expect(recipientEntity.svol_in).toBe(83200n)
    expect(recipientEntity.svol_out).toBe(102300n)
    expect(recipientEntity.stxns_in).toBe(8)
    expect(recipientEntity.stxns_out).toBe(3)
    expect(recipientEntity.sunique_in).toBe(4)
    expect(recipientEntity.sunique_out).toBe(2)
    expect(recipientEntity.sunique_all).toBe(10)
  })

  test('5) reclamation transaction, unique transaction = true', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: 'SYSTEM_ADDRESS'
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: 'SYSTEM_ADDRESS',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'reclamation'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.ovol_in).toBe(10000n)
    expect(senderEntity.ovol_out).toBe(1069012000n)
    expect(senderEntity.otxns_in).toBe(5)
    expect(senderEntity.otxns_out).toBe(11)
    expect(senderEntity.ounique_in).toBe(1)
    expect(senderEntity.ounique_out).toBe(3)
    expect(senderEntity.svol_in).toBe(40000n)
    expect(senderEntity.svol_out).toBe(63000n)
    expect(senderEntity.stxns_in).toBe(3)
    expect(senderEntity.stxns_out).toBe(6)
    expect(senderEntity.sunique_in).toBe(2)
    expect(senderEntity.sunique_out).toBe(3)
    expect(senderEntity.sunique_all).toBe(8)
  })

  test('6) reclamation transaction, unique transaction = false', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: 'SYSTEM_ADDRESS'
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: 'SYSTEM_ADDRESS',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'reclamation'
    }
    const isTransactionUnique = false
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.ovol_in).toBe(10000n)
    expect(senderEntity.ovol_out).toBe(1069012000n)
    expect(senderEntity.otxns_in).toBe(5)
    expect(senderEntity.otxns_out).toBe(11)
    expect(senderEntity.ounique_in).toBe(1)
    expect(senderEntity.ounique_out).toBe(2)
    expect(senderEntity.svol_in).toBe(40000n)
    expect(senderEntity.svol_out).toBe(63000n)
    expect(senderEntity.stxns_in).toBe(3)
    expect(senderEntity.stxns_out).toBe(6)
    expect(senderEntity.sunique_in).toBe(2)
    expect(senderEntity.sunique_out).toBe(3)
    expect(senderEntity.sunique_all).toBe(8)
  })

  test('7) sender and receiver are first transaction', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.last_trade_out).toBe('2021-06-14T01:55:00Z')
    expect(senderEntity.first_trade_in_user).toBe(null)
    expect(senderEntity.first_trade_in_time).toBe(null)
    expect(recipientEntity.last_trade_out).toBe(null)
    expect(recipientEntity.first_trade_in_user).toBe('0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A')
    expect(recipientEntity.first_trade_in_time).toBe('2021-06-14T01:55:00Z')
  })

  test('8) sender is first transaction, receiver is not first transaction', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: '2021-06-12 13:50:00',
      first_trade_in_user: 'SOME_USER_ADDRESS',
      first_trade_in_time: '2021-06-12 12:02:15'
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.last_trade_out).toBe('2021-06-14T01:55:00Z')
    expect(senderEntity.first_trade_in_user).toBe(null)
    expect(senderEntity.first_trade_in_time).toBe(null)
    expect(recipientEntity.last_trade_out).toBe('2021-06-12 13:50:00')
    expect(recipientEntity.first_trade_in_user).toBe('SOME_USER_ADDRESS')
    expect(recipientEntity.first_trade_in_time).toBe('2021-06-12 12:02:15')
  })

  test('9) sender is not first transaction, receiver is first transaction', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: '2021-06-12 13:48:20',
      first_trade_in_user: 'SOME_USER_ADDRESS',
      first_trade_in_time: '2021-06-12 12:01:45'
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: null,
      first_trade_in_user: null,
      first_trade_in_time: null
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.last_trade_out).toBe('2021-06-14T01:55:00Z')
    expect(senderEntity.first_trade_in_user).toBe('SOME_USER_ADDRESS')
    expect(senderEntity.first_trade_in_time).toBe('2021-06-12 12:01:45')
    expect(recipientEntity.last_trade_out).toBe(null)
    expect(recipientEntity.first_trade_in_user).toBe('0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A')
    expect(recipientEntity.first_trade_in_time).toBe('2021-06-14T01:55:00Z')
  })

  test('10) sender and receiver are not first transaction', () => {
    const senderEntity = {
      blockchain_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      ovol_in: 10000n,
      ovol_out: 12000n,
      otxns_in: 5,
      otxns_out: 10,
      ounique_in: 1,
      ounique_out: 2,
      svol_in: 40000n,
      svol_out: 63000n,
      stxns_in: 3,
      stxns_out: 6,
      sunique_in: 2,
      sunique_out: 3,
      sunique_all: 8,
      last_trade_out: '2021-06-12 13:48:20',
      first_trade_in_user: 'SOME_USER_ADDRESS',
      first_trade_in_time: '2021-06-12 12:01:45'
    }
    const recipientEntity = {
      blockchain_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      ovol_in: 50000n,
      ovol_out: 92800n,
      otxns_in: 10,
      otxns_out: 5,
      ounique_in: 3,
      ounique_out: 1,
      svol_in: 83200n,
      svol_out: 102300n,
      stxns_in: 8,
      stxns_out: 3,
      sunique_in: 4,
      sunique_out: 2,
      sunique_all: 10,
      last_trade_out: '2021-06-12 13:50:00',
      first_trade_in_user: 'SOME_USER_ADDRESS',
      first_trade_in_time: '2021-06-12 12:02:15'
    }
    const transactionEntity = {
      block_time: '2021-06-14T01:55:00Z',
      sender_address: '0x1304570F2e575D29422bF4753b2c6Cb493C5dc5A',
      receiver_address: '0xe21bC4B74DF35208Ee67Da0313776B99959782Aa',
      amount_sent: 1069000000,
      amount_received: 1069000000,
      transaction_type: 'standard'
    }
    const isTransactionUnique = true
    attachTransactionInfoToUser(
      senderEntity,
      recipientEntity,
      transactionEntity,
      isTransactionUnique
    )
    expect(senderEntity.last_trade_out).toBe('2021-06-14T01:55:00Z')
    expect(senderEntity.first_trade_in_user).toBe('SOME_USER_ADDRESS')
    expect(senderEntity.first_trade_in_time).toBe('2021-06-12 12:01:45')
    expect(recipientEntity.last_trade_out).toBe('2021-06-12 13:50:00')
    expect(recipientEntity.first_trade_in_user).toBe('SOME_USER_ADDRESS')
    expect(recipientEntity.first_trade_in_time).toBe('2021-06-12 12:02:15')
  })
})
