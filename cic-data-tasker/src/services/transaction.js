import pool from '../../lib/pg.js'
import * as transactionModel from '../models/transaction.js'
import { toTransactionEntity } from '../models/transactionEntity.js'
import * as userModel from '../models/user.js'

export const processTransactions = async transactions => {
  if (transactions == null || transactions.length === 0) {
    return
  }

  try {
    let transactionCount = 1
    console.log(`Received ${transactions.length} transactions`)

    for (const transaction of transactions) {
      const existingTransaction = await transactionModel.findTransaction(transaction)

      if (transactionCount % 100 == 0)
        console.log(`Processed transaction number ${transactionCount}`)
      transactionCount++

      if (existingTransaction) {
        transactionCount--
        continue
      }
      const senderEntity = await userModel.findUserByBlockchainAddress(transaction.sender)
      const recipientEntity = await userModel.findUserByBlockchainAddress(transaction.recipient)

      if (senderEntity == null || recipientEntity == null) {
        if (senderEntity === null) {
          console.log(
            'processTransactions -> senderEntity not found in DB -> blockchain_address=' +
              transaction.sender
          )
        }
        if (recipientEntity === null) {
          console.log(
            'processTransactions -> recipientEntity not found in DB -> blockchain_address=' +
              transaction.recipient
          )
        }
        continue
      }

      // set transaction type - https://gitlab.com/grassrootseconomics/cic-data/-/issues/4
      // TODO: confirm can use fixed addresses rather than reading tx_type in issue above
      setTransactionType(transaction)
      const isTransactionUnique = await transactionModel.isTransactionUnique(
        senderEntity.blockchain_address,
        recipientEntity.blockchain_address
      )
      const transactionEntity = toTransactionEntity(transaction, senderEntity, recipientEntity)

      const client = await pool.connect()
      try {
        await client.query('BEGIN')
        await transactionModel.createTransaction(client, transactionEntity)

        attachTransactionInfoToUser(
          senderEntity,
          recipientEntity,
          transactionEntity,
          isTransactionUnique
        )

        if (transactionEntity.transaction_type === 'standard') {
          await userModel.updateUser(client, senderEntity)
          await userModel.updateUser(client, recipientEntity)
        } else if (transactionEntity.transaction_type === 'disbursement') {
          await userModel.updateUser(client, recipientEntity)
        } else if (transactionEntity.transaction_type === 'reclamation') {
          await userModel.updateUser(client, senderEntity)
        }
        await client.query('COMMIT')
      } catch (e) {
        await client.query('ROLLBACK')
        console.log('processTransactions DB TRANSACTION error ->', e)
        return
      } finally {
        client.release()
      }
    }
    console.log(`Processed ${transactionCount - 1} transactions`)
  } catch (err) {
    throw `processTransactions error -> ${err}`
  }
}

const setTransactionType = transaction => {
  if (
    transaction.sender === process.env.TRUST_ADDRESS ||
    transaction.sender === process.env.FAUCET_ADDRESS
  ) {
    transaction.tx_type = 'disbursement'
  } else if (
    transaction.receiver === process.env.TRUST_ADDRESS ||
    transaction.receiver === process.env.FAUCET_ADDRESS
  ) {
    transaction.tx_type = 'reclamation'
  } else {
    transaction.tx_type = 'standard'
  }
}

export const attachTransactionInfoToUser = (
  senderEntity,
  recipientEntity,
  transactionEntity,
  isTransactionUnique
) => {
  senderEntity.last_trade_out = transactionEntity.block_time

  if (transactionEntity.transaction_type === 'standard') {
    senderEntity.svol_out = BigInt(senderEntity.svol_out) + BigInt(transactionEntity.amount_sent)
    senderEntity.stxns_out += 1

    // check if receiving User has had first receipt or not from another User
    if (!recipientEntity.first_trade_in_user) {
      recipientEntity.first_trade_in_user = senderEntity.blockchain_address
      recipientEntity.first_trade_in_time = transactionEntity.block_time
    }

    recipientEntity.svol_in =
      BigInt(recipientEntity.svol_in) + BigInt(transactionEntity.amount_received)
    recipientEntity.stxns_in += 1

    if (isTransactionUnique) {
      senderEntity.sunique_out += 1
      senderEntity.sunique_all += 1
      recipientEntity.sunique_in += 1
      recipientEntity.sunique_all += 1
    }
  } else if (transactionEntity.transaction_type === 'disbursement') {
    // only update recipient, because DISBURSEMENT means sender is System
    recipientEntity.ovol_in =
      BigInt(recipientEntity.ovol_in) + BigInt(transactionEntity.amount_received)
    recipientEntity.otxns_in += 1
    if (isTransactionUnique) {
      recipientEntity.ounique_in += 1
    }
  } else if (transactionEntity.transaction_type === 'reclamation') {
    // only update sender, because RECLAMATION means recipient is System
    senderEntity.ovol_out = BigInt(senderEntity.ovol_out) + BigInt(transactionEntity.amount_sent)
    senderEntity.otxns_out += 1
    if (isTransactionUnique) {
      senderEntity.ounique_out += 1
    }
  }
}
