# Setup Postgres & Grafana

## Postgres

1. Generate test data and export to CSV file
2. Upload file to web
3. Start up DB
4. Login to DB
   ```bash
   psql -h localhost -p 55432 -U grassroots -d cic_data
   ```
5. Copy CSV data into DB
   ```
   \copy users FROM PROGRAM 'curl https://pastebin.com/raw/x3jBtHab' CSV HEADER;
   ```
6. Check the count
   ```
   SELECT COUNT(*) FROM users;
   ```

## Grafana

1. Login at `localhost:3000` (user: admin, password: admin)
2. Add data source (PostgreSQL)
   ```
   Host: postgres-data:5432
   Database: cic_data
   User: grassroots
   Password: (none)
   TLS/SSL Mode: disable
   Version: 12
   ```
3. Create first dashboard
4. Add an empty panel
5. Discard it
6. Inspect Panel and then add JSON
7. Apply and then zoom to data range
