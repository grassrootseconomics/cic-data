# Sarafu Data Dashboard Specification

The aim of the Sarafu Data Dashboard is to deliver user and transaction metrics. The minimum requirements are specified below in [MVP Dashboard](#mvp-dashboard). More metrics will be defined and implemented in future specifications and releases.

- [Sarafu Data Dashboard Specification](#sarafu-data-dashboard-specification)
  - [Data Sources](#data-sources)
  - [Sourcing Data](#sourcing-data)
    - [CIC Meta Server API](#cic-meta-server-api)
    - [CIC Cache Server API](#cic-cache-server-api)
  - [Fetching Dashboard Data](#fetching-dashboard-data)
    - [Fetching User Data](#fetching-user-data)
    - [Fetching Transaction Data](#fetching-transaction-data)
    - [Polling Frequency](#polling-frequency)
  - [Storing Dashboard Data](#storing-dashboard-data)
    - [Dashboard DB Transaction Data](#dashboard-db-transaction-data)
      - [Transactions Table](#transactions-table)
    - [Dashboard DB User Data](#dashboard-db-user-data)
      - [Users Table](#users-table)
  - [Processing Data to Dashboard Metrics](#processing-data-to-dashboard-metrics)
    - [Sarafu Transaction Volume](#sarafu-transaction-volume)
      - [MVP](#mvp)
      - [Future](#future)
    - [Number of User to User Transactions](#number-of-user-to-user-transactions)
      - [MVP](#mvp-1)
      - [Future](#future-1)
    - [Number of Users per Day](#number-of-users-per-day)
      - [MVP](#mvp-2)
      - [Future](#future-2)
    - [Number of Registrations per Day](#number-of-registrations-per-day)
      - [MVP](#mvp-3)
      - [Future](#future-3)
  - [MVP Dashboard](#mvp-dashboard)
  - [References](#references)

## Data Sources

Data for the Dashboard will come from two sources:

1. CIC Meta (& USSD) Server - This includes user data such as their business category, location, gender, etc.
2. CIC Cache Server - This includes transaction data such as payer address, payee address, transaction type (e.g., P2P, disbursement, demurrage), etc.

The blockchain address serves as the primary key for each user in the Meta server DB, which can then be mapped to each user's transactions in the Cache server DB.

## Sourcing Data

### CIC Meta Server API

User data will be retrieved via an API call to the `cic-meta-server` service.

### CIC Cache Server API

Transaction data will be retrieved via an API call to the `cic-cache-server` service.

## Fetching Dashboard Data

The Dashboard will run a service that periodically pulls new/updated user information from the Meta API and new transaction data from the Cache API. The data from these two sources will be aggregated into a new database used for the Dashboard to generate metrics and graphs.

### Fetching User Data

The user data will be retrieved via an API call from a Dashboard service (`cic-data-tasker`) to the `cic-meta-server`.

At least one of the `cic-ussd-*` services or CICADA will notify another Dashboard service (`cic-data-server`) whenever a user record is added to or updated in the Meta server.

> **TODO**  
> **[Determine which services will send blockchain addresses signalling added or updated user records](https://gitlab.com/grassrootseconomics/cic-data/-/issues/6)**

The `cic-data-server` will inform the `cic-data-tasker` of all new/modified user records to retrieve from the Meta server.

### Fetching Transaction Data

The transaction data will be retrieved via an API call from the `cic-data-tasker` to the `cic-cache-server`.

New transactions will be retrieved by searching transaction data based on the blockheight (`start_block`) of the previous search. For example, if the last transaction records pulled were from block 227227, the next transaction fetch would start with block 227228.

[**API endpoint request and shape of returned data**](https://gitlab.com/grassrootseconomics/cic-cache/-/issues/12)

**Note:** There is a column in the Cache DB called `success` that is a boolean value. Only those transactions set to `true` should be written by the `cic-data-tasker` into the Dashboard DB.

> **TODO**
> **[Figure out how to handle case when user data is not available for a blockchain address found in the transactions](https://gitlab.com/grassrootseconomics/cic-data/-/issues/7)**

### Polling Frequency

The more frequently the Meta and Cache API endpoints are polled, the more up-to-date the Dashboard will be. The aim will be to pull new/updated user data and new transaction data every minute.

## Storing Dashboard Data

Data stored in the Dashboard database's `transactions` table will be an extension of the transaction data: in other words, all of the transaction data plus additional information (e.g., location, age, gender, product category, etc.) about each user (sender and receiver) to the transaction. Storing this extra information with each transaction will:

1. Make historic reporting accurate - if a user changes her location or product category, the change can be noted and applied to any future transactions for that user in the Dashboard's database while still leaving past transactions unaltered
2. Allow for more efficient queries - no table joins or needing to pull and process data from different sources at presentation time

Further, data stored in the `users` table will be an extension of the data already available about the user. See the [Data Overview](./data-overview.md) document for a description of the additional data that will be stored for a user.

> **TODO**
> **[Determine how to derive all data needed for spreadsheet](https://gitlab.com/grassrootseconomics/cic-data/-/issues/8)**
> **Setup users table with additional data for spreadsheet**

### Dashboard DB Transaction Data

The extended transaction data will be stored in the `transactions` table. Other tables can eventually be created to record a consolidated view of historic data, to enable faster lookups when obtaining data over a long period of time. For example, a summary for the lifetime of a token would be composed from the consolidated data rather than being built up from the entire history in the `transactions` table, which could eventually hold tens or hundreds of millions of records, and would therefore make compiling the data each time for presentation unrealistic and untimely.

#### Transactions Table

| Field Name                  | Source / Field              | Description                                                                               |
| --------------------------- | --------------------------- | ----------------------------------------------------------------------------------------- |
| `transaction_hash`          | Cache / `tx_hash`           | The unique ID of the transaction in the blockchain                                        |
| `block_number`              | Cache / `block_number`      | The block number in which the transaction was recorded                                    |
| `block_time`                | Cache / `date_block`        | ISO8601 timestamp when the block containing the transaction was written to the blockchain |
| `sender_address`            | Cache / `sender`            | The blockchain address associated to the user who created the transaction                 |
| `receiver_address`          | Cache / `recipient`         | The blockchain address associated to the user who received the transaction                |
| `amount_sent`               | Cache / `from_value`        | The amount of tokens transferred from the sender                                          |
| `amount_received`           | Cache / `to_value`          | The amount of tokens transferred to the receiver                                          |
| `transaction_type`          | Cache / `tx_type`           | Type of transaction, e.g., `standard`, `disbursement`, `reclamation`                      |
| `sender_token`              | Cache / `source_token`      | The token address on the blockchain for the token transferred by the sender               |
| `receiver_token`            | Cache / `destination_token` | The token address on the blockchain for the token transferred to the receiver             |
| `sender_registered`         | Meta / `date_registered`    | The Unix timestamp when the sender was first signed up                                    |
| `sender_product_category`   | Meta / `prod_cat`           | Type of business operated by the sender                                                   |
| `sender_gender`             | Meta / `gender`             | The gender of the sender                                                                  |
| `sender_age`                | Meta / `date_of_birth.year` | The age of the sender                                                                     |
| `sender_area_name`          | Meta / `area_name`          | The name of the area where the sender is usually located                                  |
| `sender_area_type`          | Meta / `area_type`          | The type of area (e.g., rural, urban, etc.) where the sender is usually located           |
| `receiver_registered`       | Meta / `date_registered`    | The Unix timestamp when the receiver was first signed up                                  |
| `receiver_product_category` | Meta / `prod_cat`           | Type of business operated by the receiver                                                 |
| `receiver_gender`           | Meta / `gender`             | The gender of the receiver                                                                |
| `receiver_age`              | Meta / `date_of_birth.year` | The age of the receiver                                                                   |
| `receiver_area_name`        | Meta / `area_name`          | The name of the area where the receiver is usually located                                |
| `receiver_area_type`        | Meta / `area_type`          | The type of area (e.g., rural, urban, etc.) where the receiver is usually located         |

### Dashboard DB User Data

The Dashboard database will store the user data from the Meta server DB that is needed to extend the transactions data. It will also store user-specific transaction data and other derived data about users useful to the managers of the Sarafu token program.

#### Users Table

| Field Name           | Source / Field                              | Description                                                                      |
| -------------------- | ------------------------------------------- | -------------------------------------------------------------------------------- |
| `updated_at`         | n/a                                         | The Unix timestamp of when this user record was updated from data in the Meta DB |
| `pending_update`     | n/a                                         | Boolean - flagged for update by Meta (see note)                                  |
| `blockchain_address` | Meta / `identities.evm["bloxberg:8996"][0]` | The blockchain address associated with the user                                  |
| `date_registered`    | Meta / `date_registered`                    | The Unix timestamp when the user signed up                                       |
| `product_category`   | Meta / `category`                           | Type of business operated by the user (product or service sold)                  |
| `gender`             | Meta / `gender`                             | The gender of the user                                                           |
| `date_of_birth`      | Meta / `date_of_birth`                      | The birthday of the user (object with year/month/day properties, year required)  |
| `area_name`          | Meta / `location.area_name`                 | The name of the area where the user is usually located                           |
| `area_type`          | Meta / `area_type`                          | The type of area (e.g., rural, urban, etc.) derived from `area_name`             |
| `account_type`       | Meta / `type`                               | Type of account ('individual' or 'group') **TODO - pull in from Meta**           |
| `preferred_language` | USSD / `preferred_language`                 | **TODO - make sure this is pulled in from Meta/USSD as well**                    |
| `is_market_enabled`  | USSD / `is_market_enabled`                  | **TODO - make sure this is pulled in from Meta/USSD as well**                    |

> `pending_update` is set to true when `cic-data-server` is notified by Meta to add/update the record for this `blockchain_address`. It needs to be set to false once the record has been added/updated, at which point the `updated_at` field is updated with the timestamp of when the update was made.

## Processing Data to Dashboard Metrics

The data in the `transactions` and `users` tables above will be sufficient to provide the metrics highlighted in the [MVP Dashboard](#mvp-dashboard).

### Sarafu Transaction Volume

#### MVP

Count total value (using `amount_received`) of user transactions (filtered by `transaction_type`) per calendar day (filtered by `block_time`) filtered by `area_name`.

#### Future

Same as above but filtered by `receiver_product_category`, `receiver_gender`, `receiver_age`, `receiver_area_type`.

Count total value (using `amount_received`) of disbursements and reclamations (filtered by `transaction_type`).

### Number of User to User Transactions

#### MVP

Count total number of user transactions (filtered by `transaction_type`) per calendar day (filtered by `block_time`) filtered by `area_name`.

#### Future

Same as above but filtered by `receiver_product_category`, `receiver_gender`, `receiver_age`, `receiver_area_type`.

Count disbursements and reclamations (filtered by `transaction_type`).

### Number of Users per Day

This is for users who made at least one outbound transaction.

#### MVP

Count number of unique `sender_address`es (filtered by `transaction_type`) per calendar day (filtered by `block_time`) filtered by `area_name`.

#### Future

Same as above but filtered by `sender_product_category`, `sender_gender`, `sender_age`, `sender_area_type`.

### Number of Registrations per Day

This is for new user signups.

#### MVP

Count number of new users per calendar day (filtered by `date_registered`) filtered by `area_name`.

#### Future

Same as above but filtered by `product_category`, `gender`, `age`, `area_type`.

## MVP Dashboard

![MVP Dashboard](mvp-dashboard.png)

## References

- Transaction datasets with descriptions: https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/TransactionDatasets.md
- Account and Transaction data models: https://gitlab.com/grassrootseconomics/cic-staff-client/-/blob/master/src/app/_models/account.ts & https://gitlab.com/grassrootseconomics/cic-staff-client/-/blob/master/src/app/_models/transaction.ts
- Person schema: https://gitlab.com/grassrootseconomics/cic-types/-/blob/master/cic_types/schemas/person.json
- Historic transaction data: https://www.grassrootseconomics.org/research
- [MVP Dashboard from Will](#mvp-dashboard)
- Other dashboard from Will: https://www.grassrootseconomics.org/dashboard
- Previous dashboard: https://dashboard.sarafu.network/
  ![Sarafu Dashboard](old-dashboard.png)
